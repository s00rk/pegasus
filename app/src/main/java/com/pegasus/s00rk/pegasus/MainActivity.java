package com.pegasus.s00rk.pegasus;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Date;

import database.SQLiteHelper;
import database.TempConversion;

public class MainActivity extends AppCompatActivity {

    TextView lblResultado;
    EditText txtCelsius;
    Button btnConvertir;
    String fahrenheit;

    SQLiteHelper db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnConvertir = (Button)findViewById(R.id.btnConvertir);
        lblResultado = (TextView)findViewById(R.id.lblResultado);
        txtCelsius = (EditText)findViewById(R.id.txtCelsius);

        btnConvertir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                convertirTemperatura();
            }
        });

        Log.e("KSOAP", "asd");
        db = new SQLiteHelper(this);
        TempConversion in = new TempConversion(db);
        Log.e("KSOAP", ""+in.getAll().length);
        in.setFecha(new Date());
        in.setFahrenheit(22);
        in.setCelsius(23);
        in.save();
        Log.e("KSOAP", ""+in.getAll().length);
    }

    private void convertirTemperatura(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                kSoap ex = new kSoap();
                fahrenheit = ex.getCelsiusConversion(txtCelsius.getText().toString());
                handler.sendEmptyMessage(0);
            }
        }).start();
    }

    public Handler handler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {

                case 0:
                    lblResultado.setText("ºF: " + fahrenheit);
                    TempConversion in = new TempConversion(db);
                    in.setCelsius(Float.valueOf(txtCelsius.getText().toString()));
                    in.setFahrenheit(Float.valueOf(fahrenheit));
                    in.setFecha(new Date());
                    in.save();

                    TempConversion []tt = in.getAll();
                    for(int i = 0; i < tt.length; i++)
                        Log.e("DB", String.valueOf(tt[i].getCelsius()));
                    break;
            }
            return false;
        }
    });

}
