package com.pegasus.s00rk.pegasus;

import android.util.Log;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.OutputStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 * Created by s00rk on 21/10/16.
 */

public class kSoap {

    String SOAP_ACTION = "http://www.w3schools.com/webservices/CelsiusToFahrenheit";
    String METHOD_NAME = "CelsiusToFahrenheit";
    String NAMESPACE = "http://tempuri.org/";
    String URL_R = "http://www.w3schools.com/webservices/tempconvert.asmx";

    public String getCelsiusConversion(String cValue) {
        try
        {
            SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            HttpTransportSE androidHttpTransport = new HttpTransportSE(URL_R);

            request.addProperty("Celsius", cValue);
            envelope.setOutputSoapObject(request);
            androidHttpTransport.call(SOAP_ACTION, envelope);

            Object result = envelope.getResponse();
            return result.toString();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return  "-1";
    }

}
