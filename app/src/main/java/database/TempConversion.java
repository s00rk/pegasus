package database;

/**
 * Created by s00rk on 21/10/16.
 */
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.util.Log;

import java.text.ParseException;
import java.util.Date;
import java.text.SimpleDateFormat;


public class TempConversion {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    SQLiteHelper mDbHelper;
    SQLiteDatabase db;
    Context ctx;

    private long id = 0;
    private float celsius;
    private float fahrenheit;
    private Date fecha;

    public TempConversion(SQLiteHelper db) {
        this.mDbHelper = db;
    }

    public TempConversion(SQLiteHelper db, float celsius, float fahrenheit)
    {
        this.celsius = celsius;
        this.fahrenheit = fahrenheit;
        this.mDbHelper = db;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public float getFahrenheit() {
        return fahrenheit;
    }

    public void setFahrenheit(float fahrenheit) {
        this.fahrenheit = fahrenheit;
    }

    public float getCelsius() {
        return celsius;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public void setCelsius(float celsius) {
        this.celsius = celsius;
    }

    public boolean getById(int id)
    {
        db = mDbHelper.getReadableDatabase();

        String[] projection = {
                TempConversionEntry._ID,
                TempConversionEntry.COLUMN_NAME_DATE,
                TempConversionEntry.COLUMN_NAME_CELSIUS,
                TempConversionEntry.COLUMN_NAME_FAHRENHEIT
        };

        String selection = TempConversionEntry._ID + " = ?";
        String[] selectionArgs = { String.valueOf(id) };

        Cursor c = db.query(
                TempConversionEntry.TABLE_NAME,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                null
        );
        if(c.getCount() != 1)
            return false;
        c.moveToFirst();
        this.id = c.getLong(c.getColumnIndexOrThrow(TempConversionEntry._ID));

        try {
            this.fecha = sdf.parse(c.getString(c.getColumnIndexOrThrow(TempConversionEntry.COLUMN_NAME_DATE)));
        }catch(ParseException e){
            e.printStackTrace();
        }

        this.celsius = c.getLong(c.getColumnIndexOrThrow(TempConversionEntry.COLUMN_NAME_CELSIUS));
        this.fahrenheit = c.getLong(c.getColumnIndexOrThrow(TempConversionEntry.COLUMN_NAME_FAHRENHEIT));
        return true;
    }

    public TempConversion[] getAll()
    {
        db = mDbHelper.getReadableDatabase();

        String[] projection = {
                TempConversionEntry._ID,
                TempConversionEntry.COLUMN_NAME_DATE,
                TempConversionEntry.COLUMN_NAME_CELSIUS,
                TempConversionEntry.COLUMN_NAME_FAHRENHEIT
        };

        Cursor c = db.query(
                TempConversionEntry.TABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                null
        );
        TempConversion []temps = new TempConversion[c.getCount()];
        int i = 0;
        if(c.getCount() > 0 && c.moveToFirst())
            do{
                TempConversion t = new TempConversion(mDbHelper);
                t.setId( c.getLong(c.getColumnIndexOrThrow(TempConversionEntry._ID)) );

                try {
                    t.setFecha( sdf.parse(c.getString(c.getColumnIndexOrThrow(TempConversionEntry.COLUMN_NAME_DATE))) );
                }catch(ParseException e){
                    e.printStackTrace();
                }

                t.setCelsius( c.getLong(c.getColumnIndexOrThrow(TempConversionEntry.COLUMN_NAME_CELSIUS)) );
                t.setFahrenheit( c.getLong(c.getColumnIndexOrThrow(TempConversionEntry.COLUMN_NAME_FAHRENHEIT)) );

                temps[i] = t;
            }while (c.moveToNext());
        return  temps;
    }

    public void save(){
        if(this.id != 0)
            return;
        db = mDbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TempConversionEntry.COLUMN_NAME_CELSIUS, celsius);
        values.put(TempConversionEntry.COLUMN_NAME_FAHRENHEIT, fahrenheit);
        values.put(TempConversionEntry.COLUMN_NAME_DATE, sdf.format(fecha).toString());

        Log.e("DB", values.toString());
        long newRowId = db.insert(TempConversionEntry.TABLE_NAME, null, values);
        this.id = newRowId;
    }

    public void update(){
        db = mDbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TempConversionEntry.COLUMN_NAME_CELSIUS, celsius);
        values.put(TempConversionEntry.COLUMN_NAME_FAHRENHEIT, fahrenheit);
        values.put(TempConversionEntry.COLUMN_NAME_DATE, sdf.format(fecha).toString());

        String []where = { String.valueOf(this.id) };
        db.update(TempConversionEntry.TABLE_NAME, values, TempConversionEntry._ID + " = ?", where);
    }

    public void delete(){
        db = mDbHelper.getWritableDatabase();
        String []where = { String.valueOf(this.id) };
        db.delete(TempConversionEntry.TABLE_NAME, TempConversionEntry._ID + " = ?", where);
    }

    public static class TempConversionEntry implements BaseColumns {
        public static final String TABLE_NAME = "tempconversion";
        public static final String COLUMN_NAME_DATE = "fecha";
        public static final String COLUMN_NAME_CELSIUS = "celsius";
        public static final String COLUMN_NAME_FAHRENHEIT = "fahrenheit";
    }

    private static final String TEXT_TYPE = " TEXT";
    private static final String DATE_TYPE = " DATE";
    private static final String COMMA_SEP = ",";
    public static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + TempConversionEntry.TABLE_NAME + " (" +
                    TempConversionEntry._ID + " INTEGER PRIMARY KEY," +
                    TempConversionEntry.COLUMN_NAME_DATE + DATE_TYPE + COMMA_SEP +
                    TempConversionEntry.COLUMN_NAME_CELSIUS + TEXT_TYPE + COMMA_SEP +
                    TempConversionEntry.COLUMN_NAME_FAHRENHEIT + TEXT_TYPE + " );";

    public static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + TempConversionEntry.TABLE_NAME + ";";

    public static final String getCreateSQL()
    {
        return SQL_CREATE_ENTRIES;
    }
    public static final String getDropSQL()
    {
        return SQL_DELETE_ENTRIES;
    }
}
